#customize prompt
function fish_greeting
    echo ""
    neofetch --ascii ~/.config/neofetch/goomy_colored_final.txt --ascii_colors 104 7 2 103
    set_color normal
end

#svn diff alias
function svn
    if [  "$argv[1]" = "diff" ]
        command svn $argv | nvim -R -
    else
        command svn $argv
    end
end

function projectsess
  set sess_name $argv[1]
  set starting_folder $argv[2]

  if tmux has-session -t $sess_name
    if [ -n "$TMUX" ]
      tmux detach -E "tmux attach -t $sess_name"
    else
      tmux attach -t $sess_name
    end
  else
    if [ -n "$TMUX" ]
      tmux detach -E "tmux new -s $sess_name -c $starting_folder"
    else
      tmux new -s $sess_name -c $starting_folder
    end
  end
end

create_sessions
sidshortcuts

set -gx EDITOR nvim

function fish_user_key_bindings
  fish_vi_key_bindings
  fzf_key_bindings
  for mode in insert default visual
     bind -M $mode \cf forward-char
     bind -M $mode \ck kill-line
     bind -M $mode \cu backward-kill-line
     bind -M $mode \ca beginning-of-line
     bind -M $mode \ce end-of-line
  end
end

# path
set -gx PATH "$HOME/.bin" "/usr/local/bin" $PATH

# mac specific
if test (uname) = 'Darwin'
  set -gx PATH '/Users/achen/Library/Python/3.7/bin' $PATH
end

# fzf
# use rg --files to list files so we can ignore files marked ignore in Git, svn, etc.
set -x FZF_DEFAULT_COMMAND 'rg --files'

# aliases
if test -n "$NVIM_LISTEN_ADDRESS"
  alias nvim='echo "No nesting! Try using nvr."'
end
