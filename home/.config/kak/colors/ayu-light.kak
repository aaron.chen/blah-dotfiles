evaluate-commands %sh{
   black='rgb:000000'

   blue_light='rgb:36a2d9'

   green_light='rgb:86b300'

   grey_very_very_light='rgb:fafafa'
   grey_very_light='rgb:dedede'
   grey_light='rgb:aaafb5'
   grey_med='rgb:787878'
   grey_dark='rgb:333333'
   grey_very_dark='rgb:212121'

   orange_light='rgb:e7b774'
   orange_dark='rgb:fa8c3d'

   teal_dark='rgb:5d6874'

	violet_light='rgb:a37acd'

    echo "
        # code
        face global value              ${violet_light}
        face global type               ${orange_light}
        face global variable           ${blue_light}
        face global module             ${violet_light}
        face global function           ${blue_light}
        face global string             ${green_light}
        face global keyword            ${orange_dark}+b
        face global operator           ${grey_very_dark}
        face global attribute          ${blue_light}
        face global comment            ${grey_light}
        face global meta               ${grey_med}
        face global builtin            ${blue_light}+b
        face global error              default,${black}

        # text
        face global title              ${black},default+b
        face global header             ${black},default
        face global bold               ${black},default+b
        face global italic             ${grey_very_dark},default+i
        face global mono               ${grey_dark},${grey_very_dark}
        face global block              ${grey_dark},${grey_very_dark}
        face global link               ${black},default
        face global bullet             ${black},default
        face global list               ${grey_very_dark},default

        # builtin
        face global Default            ${teal_dark},${grey_very_very_light}
    "
}

