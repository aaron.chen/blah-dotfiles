if has('termguicolors')
  set termguicolors
  let ayucolor="dark"
  colorscheme ayu
  let $BAT_THEME='Monokai Extended Light'
endif

set mouse=a

set number
set relativenumber

set hlsearch
set incsearch
set ignorecase
set smartcase
set history=300

set shiftwidth=2
set softtabstop=2
set expandtab
set smartindent
set autoindent

set hid

set colorcolumn=80

filetype on
filetype plugin on
filetype indent on
syntax on

au FileType * set fo-=c fo-=r fo-=o

set wildmenu

highlight Comment cterm=italic gui=italic

set cmdheight=2

set splitright
set splitbelow
set noequalalways


if has('nvim') && executable('nvr')
   let $GIT_EDITOR = 'nvr -cc split --remote-wait'
   augroup nvr
     autocmd!
     autocmd FileType gitcommit,gitrebase,gitconfig set bufhidden=delete
   augroup end
end

augroup restorecursor
  autocmd!
  autocmd BufReadPost *
    \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
    \ |   exe "normal! g`\""
    \ | endif
augroup end

let g:camelcasemotion_key = '\'
