set nocompatible

map <Space> <Nop>
let mapleader=" "
set timeoutlen=4000

if !empty(glob('/usr/bin/fish'))
  set shell=/usr/bin/fish
elseif !empty(glob('/usr/local/bin/fish'))
  set shell=/usr/local/bin/fish
else
  set shell=/bin/bash
endif

if has("unix")
  let s:uname = system("uname")
  if s:uname == "Darwin\n"
    let g:python2_host_prog = '/usr/local/bin/python'
    let g:python3_host_prog = '/usr/local/bin/python3'
  else
    let g:python_host_prog = '/usr/bin/python2'
    let g:python3_host_prog = '/usr/bin/python3'
  endif
endif

if has('nvim')
  if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
    silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
      \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
  endif
else
  if empty(glob('~/.vim/autoload/plug.vim'))
    silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
      \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
  endif
endif

runtime macros/matchit.vim

call plug#begin()
  Plug 'ayu-theme/ayu-vim'
  Plug 'bkad/CamelCaseMotion'
  Plug 'dag/vim-fish'
  Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
  Plug 'junegunn/fzf.vim'
  Plug 'tomtom/tcomment_vim'
  Plug 'tpope/vim-surround'
  Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'
  Plug 'vim-scripts/ReplaceWithRegister'
  Plug 'wellle/targets.vim'
call plug#end()

source ~/git/blah-dotfiles/home/.config/nvim/my_mappings.vim
source ~/git/blah-dotfiles/home/.config/nvim/my_settings.vim
