nnoremap <Leader>J J

nnoremap J 5j
nnoremap K 5k

nnoremap <leader>tt :tabnew<CR>
nnoremap <leader>tn :tabnew %<CR>
nnoremap H :tabprev<CR>
nnoremap L :tabnext<CR>
noremap <Leader>H :tabmove -1<CR>
noremap <Leader>L :tabmove +1<CR>
nnoremap <silent> <leader>0 :tabnext 1<CR>
nnoremap <silent> <leader>1 :tabnext 2<CR>
nnoremap <silent> <leader>2 :tabnext 3<CR>
nnoremap <silent> <leader>3 :tabnext 4<CR>
nnoremap <silent> <leader>4 :tabnext 5<CR>
nnoremap <silent> <leader>5 :tabnext 6<CR>
nnoremap <silent> <leader>6 :tabnext 7<CR>
nnoremap <silent> <leader>7 :tabnext 8<CR>
nnoremap <silent> <leader>8 :tabnext 9<CR>
nnoremap <silent> <leader>9 :tabnext 10<CR>

noremap <Leader>\|\| :vsplit<CR>
noremap <Leader>--   :split<CR>
noremap <Leader>wh :wincmd h<CR>
noremap <Leader>wH :wincmd H<CR>
noremap <Leader>wj :wincmd j<CR>
noremap <Leader>wJ :wincmd J<CR>
noremap <Leader>wk :wincmd k<CR>
noremap <Leader>wK :wincmd K<CR>
noremap <Leader>wl :wincmd l<CR>
noremap <Leader>wL :wincmd L<CR>
noremap <Leader>wo :only<CR>
noremap <Leader>wc :close<CR>

nnoremap n nzz
nnoremap N Nzz
nnoremap * *zz
nnoremap # #zz
nnoremap g* g*zz
nnoremap g# g#zz
noremap <Leader>w= <C-w>=
noremap <Leader>w_ <C-w>_
noremap <Leader>w\| <C-w>\|

if has('nvim')
  tnoremap <Esc> <C-\><C-n>
  tnoremap <C-v><Esc> <Esc>
endif

noremap <Leader>qo :copen<CR>
noremap <Leader>qc :cclose<CR>
noremap [q :cprevious<CR>
noremap ]q :cnext<CR>

noremap <Leader>lo :lopen<CR>
noremap <Leader>lc :lclose<CR>
noremap [l :lprevious<CR>
noremap ]l :lnext<CR>

noremap <Leader>/ :noh<CR>
noremap <Leader>x "+
nnoremap <Leader>xyfn :let @+ = expand('%')<CR>

nmap <silent> \w <Plug>CamelCaseMotion_w
nmap <silent> \b <Plug>CamelCaseMotion_b
nmap <silent> \e <Plug>CamelCaseMotion_e
nmap <silent> \ge <Plug>CamelCaseMotion_ge
omap <silent> i\w <Plug>CamelCaseMotion_iw
xmap <silent> i\w <Plug>CamelCaseMotion_iw
omap <silent> i\b <Plug>CamelCaseMotion_ib
xmap <silent> i\b <Plug>CamelCaseMotion_ib
omap <silent> i\e <Plug>CamelCaseMotion_ie
xmap <silent> i\e <Plug>CamelCaseMotion_ie

